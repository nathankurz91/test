/*
author: nathankurz
*/
package main

import (
	"fmt"
	"math"
	"time"
)

func main() {
	x := 0

	if x < 1 {
		fmt.Println("As intended my friend")
	}

	// for loop
	sum := 0
	for i := 0; i < 10; i++ {
		sum += i
	}

	fmt.Println(sum)

	// concat
	fmt.Println("go" + "lang")

	fmt.Println("1+1=", 1+1)

	fmt.Println("10/2=", 10/2)
	fmt.Println(true && false)
	fmt.Println(true)
	fmt.Println(true || false)
	fmt.Println(!true)

	var name = "nathankurz"
	fmt.Println(name)

	var one, two = 1, 2
	fmt.Println(one, two)
	fmt.Println(one + two)

	var foo = true
	fmt.Println(foo)

	var e int
	fmt.Println(e)

	little := "short"
	fmt.Println(little)

	// constants
	const s = "This string is constant"
	fmt.Println(s)

	const n = 69000000
	fmt.Println(n)

	const newN = 5e50 / n
	fmt.Println(newN)

	fmt.Println(int64(n))

	fmt.Println(math.Sin(n))

	fmt.Println("\nFor loops")

	i := 1
	for i <= 3 {
		fmt.Println(i)
		i = i + 1
	}

	for j := 7; j <= 9; j++ {
		fmt.Println(j)
	}

	for n := 0; n <= 5; n++ {
		if n%2 == 0 {
			continue
		}
		fmt.Println(n)
	}

	// if else
	j := 25
	if j < 0 {
		fmt.Println("j is negative")
	} else if j < 10 {
		fmt.Print("j is a single digit number")
	} else {
		fmt.Println("j is huuuuuuuge")
	}

	// switch
	switch time.Now().Weekday() {
	case time.Saturday, time.Sunday:
		fmt.Println("It's the weekend")
	default:
		fmt.Println("It's a weekday")
	}

	t := time.Now()
	switch {
	case t.Hour() < 12:
		fmt.Println("It's still morning")
	default:
		fmt.Println("It's already afternoon!!!")
	}

	// arrays

	var a [5]int
	fmt.Println("emp:", a)

	a[4] = 100
	fmt.Println("set:", a)
	fmt.Println("get", a[4])

	fmt.Println("len:", len(a))

	// one line array
	b := [5]int{1, 2, 3, 4, 5}
	fmt.Println("dcl:", b)

	// 2d array
	var twoD [2][3]int
	for i := 0; i < 2; i++ {
		for j := 0; j < 3; j++ {
			twoD[i][j] = i + j
		}
	}
	fmt.Println("2d: ", twoD)

	// Slices
	slice := make([]string, 3)
	fmt.Println("emp:", slice)

	slice[0] = "a"
	slice[1] = "b"
	slice[2] = "c"
	fmt.Println("set: ", slice)
	fmt.Println("get: ", slice[2])

	fmt.Println("len: ", len(slice))

	slice = append(slice, "d")
	slice = append(slice, "e", "f")
	fmt.Println("apd: ", slice)

	// copy slice
	copyOfSlice := make([]string, len(slice))
	copy(copyOfSlice, slice)
	fmt.Println("cpy: ", copyOfSlice)

	l := slice[2:5]
	fmt.Println("sl1: ", l)

	l = slice[:5]
	fmt.Println("sl2: ", l)

	l = slice[2:]
	fmt.Println("sl3: ", l)

	// single line slice
	oneLiner := []string{"g", "h", "i"}
	fmt.Println("dcl: ", oneLiner)

	// twoD slice
	twoDslice := make([][]int, 3)
	for i := 0; i < 3; i++ {
		innerLen := i + 1
		twoDslice[i] = make([]int, innerLen)
		for j := 0; j < innerLen; j++ {
			twoDslice[i][j] = i + j
		}
	}

	fmt.Println("2d: ", twoDslice)

}
